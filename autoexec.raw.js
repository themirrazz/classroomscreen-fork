fetch("https://raw.githubusercontent.com/themirrazz/themirrazz.github.io/main/classroomscreenfork-index.js")
.then(e=>e.txt)
.then
(async(index)=>{
  eval(await (await fetch("https://polyfill.io/v3/polyfill.min.js?version=3.111.0&features=Array.from%2CArray.isArray%2CArray.prototype.entries%2CArray.prototype.every%2CArray.prototype.fill%2CArray.prototype.filter%2CArray.prototype.find%2CArray.prototype.findIndex%2CArray.prototype.flatMap%2CArray.prototype.forEach%2CArray.prototype.includes%2CArray.prototype.indexOf%2CArray.prototype.keys%2CArray.prototype.lastIndexOf%2CArray.prototype.map%2CArray.prototype.reduce%2CArray.prototype.some%2CArray.prototype.sort%2CArray.prototype.values%2CArrayBuffer%2Catob%2CAudioContext%2CBlob%2Cconsole%2CCustomEvent%2CDataView%2CDate.now%2CDate.prototype.toISOString%2Cdocument%2CDOMTokenList%2CElement%2CEvent%2Cfetch%2CFloat32Array%2CFunction.prototype.bind%2CgetComputedStyle%2CglobalThis%2CIntersectionObserver%2CJSON%2ClocalStorage%2CMap%2CMath.hypot%2CMath.sign%2CMath.trunc%2Cmodernizr:es5object%2CMutationObserver%2CNumber.isInteger%2CNumber.isNaN%2CNumber.MAX_SAFE_INTEGER%2CNumber.MIN_SAFE_INTEGER%2CNumber.parseFloat%2CNumber.parseInt%2CObject.assign%2CObject.entries%2CObject.freeze%2CObject.getOwnPropertyDescriptors%2CObject.getOwnPropertySymbols%2CObject.is%2CObject.isExtensible%2CObject.setPrototypeOf%2CObject.values%2CPromise%2CPromise.prototype.finally%2CqueueMicrotask%2CReflect%2CReflect.construct%2CReflect.deleteProperty%2CReflect.get%2CReflect.getPrototypeOf%2CReflect.has%2CReflect.ownKeys%2CReflect.set%2CRegExp.prototype.flags%2CrequestAnimationFrame%2CResizeObserver%2CSet%2CString.prototype.endsWith%2CString.prototype.includes%2CString.prototype.matchAll%2CString.prototype.normalize%2CString.prototype.padStart%2CString.prototype.startsWith%2CString.prototype.trim%2CSymbol%2CSymbol.iterator%2CSymbol.prototype.description%2CSymbol.toStringTag%2CSymbol.unscopables%2CTextDecoder%2CUint16Array%2CUint8Array%2CURL%2CURLSearchParams%2CWeakMap%2CWeakSet%2CXMLHttpRequest%2CPromise.any%2CPromise.allSettled")).text())
  await import('/app/vendor-8eeedc94.js')
        document.head.innerHTML=`<meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="mobile-web-app-capable" content="yes" />
    <meta name="apple-touch-fullscreen" content="yes" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
    <meta name="google" content="notranslate" />

    <script src=""></script>

    
    <script type="module" crossorigin src="/app/index-c76b2691.js"></script>
    <link rel="modulepreload" href="/app/vendor-8eeedc94.js">
    <link rel="stylesheet" href="/app/style-6f0467e9.css">`;
    document.body.innerHTML=`<div
      id="app-splash"
      style="
        position: fixed;
        top: 0;
        left: 0;
        height: 100%;
        width: 100vw;
        z-index: 2147483647;
        background: white;
        overflow: hidden;
        display: flex;
        justify-content: center;
        align-items: center;
      "
    >
      <svg width="116" height="116" viewBox="0 0 116 116" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path class="crs-splash-outer-circle" d="M108 58C108 85.6142 85.6142 108 58 108C30.3858 108 8 85.6142 8 58C8 30.3858 30.3858 8 58 8C85.6142 8 108 30.3858 108 58Z" />
        <path class="crs-splash-outer-circle-up" d="M108 58C108 85.6142 85.6142 108 58 108C30.3858 108 8 85.6142 8 58C8 30.3858 30.3858 8 58 8C85.6142 8 108 30.3858 108 58Z" />
        <path class="crs-splash-inner-circle" d="M81 58C81 70.7025 70.7025 81 58 81C45.2975 81 35 70.7025 35 58C35 45.2975 45.2975 35 58 35C70.7025 35 81 45.2975 81 58Z" />
        <path class="crs-splash-inner-circle-up" d="M81 58C81 70.7025 70.7025 81 58 81C45.2975 81 35 70.7025 35 58C35 45.2975 45.2975 35 58 35C70.7025 35 81 45.2975 81 58Z" />
      </svg>
      <p
        id="app-splash-text-initial"
        style="
          position: absolute;
          bottom: 32px;
          margin: 0 !important;
          line-height: 16px !important;
          height: 16px !important;
          color: #1f2937;
          font-weight: 400;
          font-size: 14px;
          font-family: 'Quicksand', 'Verdana', 'Helvetica', 'Arial', sans-serif;
        "
      >
        Loading resources
      </p>
      <p id="app-splash-text" class="crs-body" style="position: absolute; bottom: 32px; margin: 0 !important; line-height: 16px !important; height: 16px !important"></p>
    </div>
    <div id="app-root">
      <div id="app"></div>
      <div id="app-teleport"></div>
      <div id="object-store"></div>
    </div>
    `;
    await import (
      'data:text/javascript;base64,'+btoa(index)
    )
});
